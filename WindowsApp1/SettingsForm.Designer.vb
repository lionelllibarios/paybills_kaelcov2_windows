﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblBranch = New System.Windows.Forms.Label()
        Me.lblCashierName = New System.Windows.Forms.Label()
        Me.lblSettings = New System.Windows.Forms.Label()
        Me.txtCashierName = New System.Windows.Forms.TextBox()
        Me.txtBranch = New System.Windows.Forms.TextBox()
        Me.btnSaveSettings = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblBranch
        '
        Me.lblBranch.AutoSize = True
        Me.lblBranch.Location = New System.Drawing.Point(9, 92)
        Me.lblBranch.Name = "lblBranch"
        Me.lblBranch.Size = New System.Drawing.Size(44, 13)
        Me.lblBranch.TabIndex = 10
        Me.lblBranch.Text = "Branch:"
        Me.lblBranch.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblCashierName
        '
        Me.lblCashierName.AutoSize = True
        Me.lblCashierName.Location = New System.Drawing.Point(9, 66)
        Me.lblCashierName.Name = "lblCashierName"
        Me.lblCashierName.Size = New System.Drawing.Size(76, 13)
        Me.lblCashierName.TabIndex = 8
        Me.lblCashierName.Text = "Cashier Name:"
        Me.lblCashierName.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lblSettings
        '
        Me.lblSettings.AutoSize = True
        Me.lblSettings.Location = New System.Drawing.Point(118, 25)
        Me.lblSettings.Name = "lblSettings"
        Me.lblSettings.Size = New System.Drawing.Size(45, 13)
        Me.lblSettings.TabIndex = 7
        Me.lblSettings.Text = "Settings"
        Me.lblSettings.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtCashierName
        '
        Me.txtCashierName.Location = New System.Drawing.Point(107, 63)
        Me.txtCashierName.Name = "txtCashierName"
        Me.txtCashierName.Size = New System.Drawing.Size(165, 20)
        Me.txtCashierName.TabIndex = 6
        '
        'txtBranch
        '
        Me.txtBranch.Location = New System.Drawing.Point(107, 89)
        Me.txtBranch.Name = "txtBranch"
        Me.txtBranch.Size = New System.Drawing.Size(165, 20)
        Me.txtBranch.TabIndex = 12
        '
        'btnSaveSettings
        '
        Me.btnSaveSettings.Location = New System.Drawing.Point(12, 153)
        Me.btnSaveSettings.Name = "btnSaveSettings"
        Me.btnSaveSettings.Size = New System.Drawing.Size(260, 23)
        Me.btnSaveSettings.TabIndex = 13
        Me.btnSaveSettings.Text = "Save"
        Me.btnSaveSettings.UseVisualStyleBackColor = True
        '
        'frmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 188)
        Me.Controls.Add(Me.btnSaveSettings)
        Me.Controls.Add(Me.txtBranch)
        Me.Controls.Add(Me.lblBranch)
        Me.Controls.Add(Me.lblCashierName)
        Me.Controls.Add(Me.lblSettings)
        Me.Controls.Add(Me.txtCashierName)
        Me.Name = "frmSettings"
        Me.Text = "Settings"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblBranch As Label
    Friend WithEvents lblCashierName As Label
    Friend WithEvents lblSettings As Label
    Friend WithEvents txtCashierName As TextBox
    Friend WithEvents txtBranch As TextBox
    Friend WithEvents btnSaveSettings As Button
End Class
