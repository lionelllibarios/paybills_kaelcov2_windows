﻿Public Class frmSettings

    Private Sub frmSettings_Load(sender As Object, e As EventArgs) Handles Me.Load
        lblSettings.Font = New Font("Arial", 12)

        ' Load Settings
        Dim cashierName = My.Settings.CashierName
        Dim branch = My.Settings.Branch

        If String.IsNullOrEmpty(cashierName) Then
            txtCashierName.SetWaterMark("Enter Cashier's Name...")
        Else
            txtCashierName.Text = cashierName
            txtCashierName.Refresh()
        End If

        If String.IsNullOrEmpty(branch) Then
            txtBranch.SetWaterMark("Enter Branch...")
        Else
            txtBranch.Text = branch
            txtBranch.Refresh()
        End If


    End Sub

    Private Sub btnSaveSettings_Click_1(sender As Object, e As EventArgs) Handles btnSaveSettings.Click
        Dim cashierName = txtCashierName.Text
        Dim branch = txtBranch.Text

        My.Settings.CashierName = cashierName
        My.Settings.Branch = branch
        My.Settings.Save()

        MsgBox("Settings Save!")
    End Sub
End Class