﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class fmPayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnPay = New System.Windows.Forms.Button()
        Me.cboPrinter = New System.Windows.Forms.ComboBox()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.lblHeader = New System.Windows.Forms.Label()
        Me.lblAccount = New System.Windows.Forms.Label()
        Me.lblName = New System.Windows.Forms.Label()
        Me.lblMobile = New System.Windows.Forms.Label()
        Me.lblAmount = New System.Windows.Forms.Label()
        Me.lblIssuance = New System.Windows.Forms.Label()
        Me.lblBillingMonth = New System.Windows.Forms.Label()
        Me.txtAccount = New System.Windows.Forms.TextBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtMobile = New System.Windows.Forms.TextBox()
        Me.txtAmount = New System.Windows.Forms.TextBox()
        Me.txtIssuance = New System.Windows.Forms.TextBox()
        Me.txtBillingMonth = New System.Windows.Forms.TextBox()
        Me.lblBalance = New System.Windows.Forms.Label()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.txtTendered = New System.Windows.Forms.TextBox()
        Me.lblTender = New System.Windows.Forms.Label()
        Me.txtAddress = New System.Windows.Forms.TextBox()
        Me.lblAddress = New System.Windows.Forms.Label()
        Me.txtMeterNo = New System.Windows.Forms.TextBox()
        Me.lblMeterNo = New System.Windows.Forms.Label()
        Me.btnSettings = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rdbCurrentOnly = New System.Windows.Forms.RadioButton()
        Me.rdbOver30 = New System.Windows.Forms.RadioButton()
        Me.rdbOver60 = New System.Windows.Forms.RadioButton()
        Me.rdbOver90 = New System.Windows.Forms.RadioButton()
        Me.rdbOver120 = New System.Windows.Forms.RadioButton()
        Me.rdbCurrent = New System.Windows.Forms.RadioButton()
        Me.txtORNumber = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnPay
        '
        Me.btnPay.Location = New System.Drawing.Point(39, 437)
        Me.btnPay.Name = "btnPay"
        Me.btnPay.Size = New System.Drawing.Size(365, 40)
        Me.btnPay.TabIndex = 0
        Me.btnPay.Text = "Transact"
        Me.btnPay.UseVisualStyleBackColor = True
        '
        'cboPrinter
        '
        Me.cboPrinter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPrinter.DropDownWidth = 229
        Me.cboPrinter.FormattingEnabled = True
        Me.cboPrinter.Location = New System.Drawing.Point(114, 410)
        Me.cboPrinter.Name = "cboPrinter"
        Me.cboPrinter.Size = New System.Drawing.Size(229, 21)
        Me.cboPrinter.TabIndex = 1
        Me.cboPrinter.Text = "Select Printer"
        '
        'PrintDocument1
        '
        '
        'lblHeader
        '
        Me.lblHeader.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblHeader.Location = New System.Drawing.Point(12, 9)
        Me.lblHeader.Name = "lblHeader"
        Me.lblHeader.Size = New System.Drawing.Size(421, 37)
        Me.lblHeader.TabIndex = 4
        Me.lblHeader.Text = "KAELCO ONLINE PAYMENT v2.9.3"
        Me.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblAccount
        '
        Me.lblAccount.Location = New System.Drawing.Point(35, 90)
        Me.lblAccount.Name = "lblAccount"
        Me.lblAccount.Size = New System.Drawing.Size(128, 23)
        Me.lblAccount.TabIndex = 5
        Me.lblAccount.Text = "Customer Account #:"
        Me.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblName
        '
        Me.lblName.Location = New System.Drawing.Point(35, 113)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(128, 23)
        Me.lblName.TabIndex = 6
        Me.lblName.Text = "Customer Name:"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMobile
        '
        Me.lblMobile.Location = New System.Drawing.Point(35, 136)
        Me.lblMobile.Name = "lblMobile"
        Me.lblMobile.Size = New System.Drawing.Size(128, 23)
        Me.lblMobile.TabIndex = 7
        Me.lblMobile.Text = "Customer Mobile #:"
        Me.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAmount
        '
        Me.lblAmount.Location = New System.Drawing.Point(36, 232)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(128, 23)
        Me.lblAmount.TabIndex = 8
        Me.lblAmount.Text = "Paying Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblIssuance
        '
        Me.lblIssuance.Location = New System.Drawing.Point(36, 255)
        Me.lblIssuance.Name = "lblIssuance"
        Me.lblIssuance.Size = New System.Drawing.Size(128, 23)
        Me.lblIssuance.TabIndex = 9
        Me.lblIssuance.Text = "Bill Issuance Date:"
        Me.lblIssuance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblBillingMonth
        '
        Me.lblBillingMonth.Location = New System.Drawing.Point(36, 278)
        Me.lblBillingMonth.Name = "lblBillingMonth"
        Me.lblBillingMonth.Size = New System.Drawing.Size(128, 23)
        Me.lblBillingMonth.TabIndex = 10
        Me.lblBillingMonth.Text = "Billing Month:"
        Me.lblBillingMonth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAccount
        '
        Me.txtAccount.Location = New System.Drawing.Point(169, 92)
        Me.txtAccount.Name = "txtAccount"
        Me.txtAccount.Size = New System.Drawing.Size(234, 20)
        Me.txtAccount.TabIndex = 11
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(169, 115)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(234, 20)
        Me.txtName.TabIndex = 12
        '
        'txtMobile
        '
        Me.txtMobile.Location = New System.Drawing.Point(169, 138)
        Me.txtMobile.Name = "txtMobile"
        Me.txtMobile.Size = New System.Drawing.Size(234, 20)
        Me.txtMobile.TabIndex = 13
        '
        'txtAmount
        '
        Me.txtAmount.Location = New System.Drawing.Point(170, 234)
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Size = New System.Drawing.Size(234, 20)
        Me.txtAmount.TabIndex = 14
        '
        'txtIssuance
        '
        Me.txtIssuance.Location = New System.Drawing.Point(170, 257)
        Me.txtIssuance.Name = "txtIssuance"
        Me.txtIssuance.Size = New System.Drawing.Size(234, 20)
        Me.txtIssuance.TabIndex = 15
        '
        'txtBillingMonth
        '
        Me.txtBillingMonth.Location = New System.Drawing.Point(170, 280)
        Me.txtBillingMonth.Name = "txtBillingMonth"
        Me.txtBillingMonth.Size = New System.Drawing.Size(234, 20)
        Me.txtBillingMonth.TabIndex = 16
        '
        'lblBalance
        '
        Me.lblBalance.Location = New System.Drawing.Point(13, 480)
        Me.lblBalance.Name = "lblBalance"
        Me.lblBalance.Size = New System.Drawing.Size(418, 52)
        Me.lblBalance.TabIndex = 17
        Me.lblBalance.Text = "Balance: Php0.00"
        Me.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblUsername
        '
        Me.lblUsername.Location = New System.Drawing.Point(12, 46)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(421, 23)
        Me.lblUsername.TabIndex = 18
        Me.lblUsername.Text = "Username"
        Me.lblUsername.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtTendered
        '
        Me.txtTendered.Location = New System.Drawing.Point(170, 303)
        Me.txtTendered.Name = "txtTendered"
        Me.txtTendered.Size = New System.Drawing.Size(234, 20)
        Me.txtTendered.TabIndex = 20
        '
        'lblTender
        '
        Me.lblTender.Location = New System.Drawing.Point(36, 301)
        Me.lblTender.Name = "lblTender"
        Me.lblTender.Size = New System.Drawing.Size(128, 23)
        Me.lblTender.TabIndex = 19
        Me.lblTender.Text = "Amount Tendered:"
        Me.lblTender.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAddress
        '
        Me.txtAddress.Location = New System.Drawing.Point(170, 326)
        Me.txtAddress.Name = "txtAddress"
        Me.txtAddress.Size = New System.Drawing.Size(234, 20)
        Me.txtAddress.TabIndex = 22
        '
        'lblAddress
        '
        Me.lblAddress.Location = New System.Drawing.Point(36, 324)
        Me.lblAddress.Name = "lblAddress"
        Me.lblAddress.Size = New System.Drawing.Size(128, 23)
        Me.lblAddress.TabIndex = 21
        Me.lblAddress.Text = "Address:"
        Me.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMeterNo
        '
        Me.txtMeterNo.Location = New System.Drawing.Point(170, 349)
        Me.txtMeterNo.Name = "txtMeterNo"
        Me.txtMeterNo.Size = New System.Drawing.Size(234, 20)
        Me.txtMeterNo.TabIndex = 24
        '
        'lblMeterNo
        '
        Me.lblMeterNo.Location = New System.Drawing.Point(36, 347)
        Me.lblMeterNo.Name = "lblMeterNo"
        Me.lblMeterNo.Size = New System.Drawing.Size(128, 23)
        Me.lblMeterNo.TabIndex = 23
        Me.lblMeterNo.Text = "Meter Number:"
        Me.lblMeterNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSettings
        '
        Me.btnSettings.Location = New System.Drawing.Point(150, 527)
        Me.btnSettings.Name = "btnSettings"
        Me.btnSettings.Size = New System.Drawing.Size(141, 23)
        Me.btnSettings.TabIndex = 25
        Me.btnSettings.Text = "Settings"
        Me.btnSettings.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(35, 159)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(128, 23)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "Paying Bill:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'rdbCurrentOnly
        '
        Me.rdbCurrentOnly.AutoSize = True
        Me.rdbCurrentOnly.Location = New System.Drawing.Point(169, 165)
        Me.rdbCurrentOnly.Name = "rdbCurrentOnly"
        Me.rdbCurrentOnly.Size = New System.Drawing.Size(99, 17)
        Me.rdbCurrentOnly.TabIndex = 27
        Me.rdbCurrentOnly.TabStop = True
        Me.rdbCurrentOnly.Text = "Current Bill Only"
        Me.rdbCurrentOnly.UseVisualStyleBackColor = True
        '
        'rdbOver30
        '
        Me.rdbOver30.AutoSize = True
        Me.rdbOver30.Location = New System.Drawing.Point(169, 188)
        Me.rdbOver30.Name = "rdbOver30"
        Me.rdbOver30.Size = New System.Drawing.Size(63, 17)
        Me.rdbOver30.TabIndex = 28
        Me.rdbOver30.TabStop = True
        Me.rdbOver30.Text = "Over 30"
        Me.rdbOver30.UseVisualStyleBackColor = True
        '
        'rdbOver60
        '
        Me.rdbOver60.AutoSize = True
        Me.rdbOver60.Location = New System.Drawing.Point(274, 187)
        Me.rdbOver60.Name = "rdbOver60"
        Me.rdbOver60.Size = New System.Drawing.Size(63, 17)
        Me.rdbOver60.TabIndex = 29
        Me.rdbOver60.TabStop = True
        Me.rdbOver60.Text = "Over 60"
        Me.rdbOver60.UseVisualStyleBackColor = True
        '
        'rdbOver90
        '
        Me.rdbOver90.AutoSize = True
        Me.rdbOver90.Location = New System.Drawing.Point(169, 211)
        Me.rdbOver90.Name = "rdbOver90"
        Me.rdbOver90.Size = New System.Drawing.Size(63, 17)
        Me.rdbOver90.TabIndex = 30
        Me.rdbOver90.TabStop = True
        Me.rdbOver90.Text = "Over 90"
        Me.rdbOver90.UseVisualStyleBackColor = True
        '
        'rdbOver120
        '
        Me.rdbOver120.AutoSize = True
        Me.rdbOver120.Location = New System.Drawing.Point(274, 210)
        Me.rdbOver120.Name = "rdbOver120"
        Me.rdbOver120.Size = New System.Drawing.Size(69, 17)
        Me.rdbOver120.TabIndex = 31
        Me.rdbOver120.TabStop = True
        Me.rdbOver120.Text = "Over 120"
        Me.rdbOver120.UseVisualStyleBackColor = True
        '
        'rdbCurrent
        '
        Me.rdbCurrent.AutoSize = True
        Me.rdbCurrent.Location = New System.Drawing.Point(274, 164)
        Me.rdbCurrent.Name = "rdbCurrent"
        Me.rdbCurrent.Size = New System.Drawing.Size(143, 17)
        Me.rdbCurrent.TabIndex = 32
        Me.rdbCurrent.TabStop = True
        Me.rdbCurrent.Text = "Current Bill w/ Surcharge"
        Me.rdbCurrent.UseVisualStyleBackColor = True
        '
        'txtORNumber
        '
        Me.txtORNumber.Location = New System.Drawing.Point(170, 372)
        Me.txtORNumber.Name = "txtORNumber"
        Me.txtORNumber.Size = New System.Drawing.Size(234, 20)
        Me.txtORNumber.TabIndex = 34
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(36, 369)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(128, 23)
        Me.Label2.TabIndex = 33
        Me.Label2.Text = "OR Number:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'fmPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(445, 564)
        Me.Controls.Add(Me.txtORNumber)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.rdbCurrent)
        Me.Controls.Add(Me.rdbOver120)
        Me.Controls.Add(Me.rdbOver90)
        Me.Controls.Add(Me.rdbOver60)
        Me.Controls.Add(Me.rdbOver30)
        Me.Controls.Add(Me.rdbCurrentOnly)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnSettings)
        Me.Controls.Add(Me.txtMeterNo)
        Me.Controls.Add(Me.lblMeterNo)
        Me.Controls.Add(Me.txtAddress)
        Me.Controls.Add(Me.lblAddress)
        Me.Controls.Add(Me.txtTendered)
        Me.Controls.Add(Me.lblTender)
        Me.Controls.Add(Me.lblUsername)
        Me.Controls.Add(Me.lblBalance)
        Me.Controls.Add(Me.txtBillingMonth)
        Me.Controls.Add(Me.txtIssuance)
        Me.Controls.Add(Me.txtAmount)
        Me.Controls.Add(Me.txtMobile)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.txtAccount)
        Me.Controls.Add(Me.lblBillingMonth)
        Me.Controls.Add(Me.lblIssuance)
        Me.Controls.Add(Me.lblAmount)
        Me.Controls.Add(Me.lblMobile)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.lblAccount)
        Me.Controls.Add(Me.lblHeader)
        Me.Controls.Add(Me.cboPrinter)
        Me.Controls.Add(Me.btnPay)
        Me.Name = "fmPayment"
        Me.Text = "Payment Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnPay As Button
    Friend WithEvents cboPrinter As ComboBox
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Friend WithEvents lblHeader As Label
    Friend WithEvents lblAccount As Label
    Friend WithEvents lblName As Label
    Friend WithEvents lblMobile As Label
    Friend WithEvents lblAmount As Label
    Friend WithEvents lblIssuance As Label
    Friend WithEvents lblBillingMonth As Label
    Friend WithEvents txtAccount As TextBox
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtMobile As TextBox
    Friend WithEvents txtAmount As TextBox
    Friend WithEvents txtIssuance As TextBox
    Friend WithEvents txtBillingMonth As TextBox
    Friend WithEvents lblBalance As Label
    Friend WithEvents lblUsername As Label
    Friend WithEvents txtTendered As TextBox
    Friend WithEvents lblTender As Label
    Friend WithEvents txtAddress As TextBox
    Friend WithEvents lblAddress As Label
    Friend WithEvents txtMeterNo As TextBox
    Friend WithEvents lblMeterNo As Label
    Friend WithEvents btnSettings As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Label1 As Label
    Friend WithEvents rdbCurrentOnly As RadioButton
    Friend WithEvents rdbOver30 As RadioButton
    Friend WithEvents rdbOver60 As RadioButton
    Friend WithEvents rdbOver90 As RadioButton
    Friend WithEvents rdbOver120 As RadioButton
    Friend WithEvents rdbCurrent As RadioButton
    Friend WithEvents txtORNumber As TextBox
    Friend WithEvents Label2 As Label
End Class
