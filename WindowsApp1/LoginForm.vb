﻿Imports System.Net
Imports System.Net.Http
Imports System.Net.NetworkInformation
Imports System.Xml
Imports Newtonsoft.Json.Linq

Public Class fmLogin
    Private Sub LoginForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtUsername.SetWaterMark("Username...")
        txtPassword.SetWaterMark("Password...")

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click

    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click

        'txtUsername.Text = "gk1tg8sq"
        'txtPassword.Text = "50b0o4vm"
        'txtUsername.Refresh()
        'txtPassword.Refresh()
        Dim txtUser = WebUtility.UrlEncode(txtUsername.Text)
        Dim txtPass = WebUtility.UrlEncode(txtPassword.Text)

        Debug.WriteLine("txtUser: " + txtUser)
        Debug.WriteLine("txtPass: " + txtPass)

        If String.IsNullOrEmpty(txtUser) Then
            MsgBox("Please Enter Username")
        ElseIf String.IsNullOrEmpty(txtPass) Then
            MsgBox("Please Enter Password")
        Else
            Dim macAddress = getMacAddress()
            Debug.WriteLine("Mac Address: " + macAddress)

            Try
                Dim lon = "0"
                Dim lat = "0"
                Dim urlLogin = "http://f8mobile.net/web_api/index.php/webservice_api/get_data?username=" + txtUser + "&password=" + txtPass + "&longitude=" + lon + "&latitude=" + lat
                Dim webClient As New System.Net.WebClient
                Dim result As String = webClient.DownloadString(urlLogin)
                'Dim xmlElem = XElement.Parse(result)

                'Dim doc = XDocument.Parse(result)

                'Dim elems = doc.Root.Elements("Result").FirstOrDefault(Function(x) x.Element("Product_id").Value = "4")

                'at this point, product contain the desired <Product> element'
                'you can get product name this way :'
                'Dim resCode As String = elems.Element("Result").Value

                Dim doc As XmlDocument = New XmlDocument()
                doc.LoadXml(result)

                Dim resCode = doc.SelectSingleNode("/F8MobileXMLRequest/Result").InnerText()

                If String.Equals(resCode, "0000") Then
                    ValidateMacAddress()
                    'Me.Hide()
                    'MessageBox.Show("Login Successful!") ' + vbNewLine + "Welcome: " + txtUsername.Text)
                    'fmPayment.Show()
                Else
                    MsgBox("Login Failed, Please check username/password.")
                End If
            Catch ex As Exception
                MsgBox("Connection Timeout")
                'MsgBox(ex.Message)
            End Try

        End If
    End Sub

    Public Async Sub ValidateMacAddress()
        Dim res = Await ValidateMacAddressTask()
        Debug.WriteLine("res: " + res)

        If String.IsNullOrEmpty(res) Then
            MsgBox("Connection Timeout.")
        Else
            Dim ser As JObject = JObject.Parse(res)
            Debug.WriteLine("ser: " + ser.ToString)
            Dim data As List(Of JToken) = ser.Children().ToList
            Dim resCode As String = ""
            Dim resMsg As String = ""

            For Each item As JProperty In data
                item.CreateReader()
                Select Case item.Name
                    Case "ResponseCode"
                        resCode = item.Value.ToString

                    Case "ResponseMessage"
                        resMsg = item.Value.ToString

                End Select
            Next

            Debug.WriteLine("lines: " + resCode + " - " + resMsg)


            If String.Equals(resCode, "0000") Then
                Me.Hide()
                MessageBox.Show("Login Successful!") ' + vbNewLine + "Welcome: " + txtUsername.Text)
                fmPayment.Show()
            Else
                MsgBox("Unauthorized access.")
            End If
        End If
    End Sub

    Async Function ValidateMacAddressTask() As Task(Of String)

        Dim urlMacAddress = WebUtility.UrlEncode(getMacAddress())

        Dim url = "https://f8mobile.net/web_api/index.php/webservice_api/validate?mac_address=" + urlMacAddress

        Debug.WriteLine("URL: " + url)

        Try
            ' You need to add a reference to System.Net.Http to declare client.  
            Dim client As HttpClient = New HttpClient()

            ' GetStringAsync returns a Task(Of String). That means that when you await the  
            ' task you'll get a string (urlContents).  
            Dim getStringTask As Task(Of String) = client.GetStringAsync(url)

            ' You can do work here that doesn't rely on the string from GetStringAsync.  
            'DoIndependentWork()

            ' The Await operator suspends AccessTheWebAsync.  
            '  - AccessTheWebAsync can't continue until getStringTask is complete.  
            '  - Meanwhile, control returns to the caller of AccessTheWebAsync.  
            '  - Control resumes here when getStringTask is complete.   
            '  - The Await operator then retrieves the string result from getStringTask.  
            Dim urlContents As String = Await getStringTask

            ' The return statement specifies an integer result.  
            ' Any methods that are awaiting AccessTheWebAsync retrieve the length value.  
            Return urlContents
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Function getMacAddress()
        Dim nics() As NetworkInterface = NetworkInterface.GetAllNetworkInterfaces()
        Return nics(0).GetPhysicalAddress.ToString
    End Function

End Class