﻿Imports System.Globalization
Imports System.Net
Imports System.Net.Http
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Xml
Imports Newtonsoft.Json.Linq

Public Class fmPayment

    Dim TextToPrint As String = ""

    Private Sub fmPayment_Load(ByVal sender As System.Object, ByVal _
    e As System.EventArgs) Handles Me.Load

        lblBalance.Font = New Font("Arial", 10)
        lblHeader.Font = New Font("Arial", 12)

        Dim txtUser = fmLogin.txtUsername.Text
        Dim txtPass = fmLogin.txtPassword.Text
        lblUsername.Text = "User: " + fmLogin.txtUsername.Text
        lblUsername.Refresh()

        txtAccount.Select()

        For Each printer As String In
            Printing.PrinterSettings.InstalledPrinters
            cboPrinter.Items.Add(printer)
        Next printer


        GetBalance()

        txtAccount.SetWaterMark("Account Number...")
        txtName.SetWaterMark("Customer Name...")
        txtMobile.SetWaterMark("639xxxxxxxxx")
        txtAmount.SetWaterMark("Amount(Php)")
        txtIssuance.SetWaterMark("MM-DD-YYYY")
        txtBillingMonth.SetWaterMark("YYYYMM")
        txtTendered.SetWaterMark("Cash Tendered...")
        txtAddress.SetWaterMark("Customer Address...")
        txtMeterNo.SetWaterMark("Customer Meter Number...")
        txtORNumber.SetWaterMark("Customer OR Number...")
    End Sub

    Private Sub cboPrinter_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboPrinter.SelectionChangeCommitted
        Dim selectedItem = cboPrinter.SelectedItem
        'MsgBox(selectedItem)
    End Sub

    ' Print.
    Private Sub btnPay_Click(ByVal sender As System.Object,
    ByVal e As System.EventArgs) Handles btnPay.Click

        If String.IsNullOrEmpty(txtAccount.Text) Then '
            MsgBox("Please Enter Customer's Account Number") '
        ElseIf String.IsNullOrEmpty(txtName.Text) Then
            MsgBox("Please Enter Customer's Name")
        ElseIf String.IsNullOrEmpty(txtMobile.Text) Then
            MsgBox("Please Enter Mobile Number")
        ElseIf String.IsNullOrEmpty(txtAmount.Text) Then
            MsgBox("Please Enter Amount")
        ElseIf String.IsNullOrEmpty(txtIssuance.Text) Then
            MsgBox("Please Enter Issuance Date")
        ElseIf String.IsNullOrEmpty(txtBillingMonth.Text) Then
            MsgBox("Please Enter Billing Month")
        ElseIf String.IsNullOrEmpty(txtTendered.Text) Then
            MsgBox("Please Enter Cash Tendered")
        ElseIf String.IsNullOrEmpty(txtMeterNo.Text) Then
            MsgBox("Please Enter Meter Number")
        ElseIf String.IsNullOrEmpty(txtORNumber.Text) Then
            MsgBox("Please Enter OR Number")
        Else
            'ValidatePayment()

            Dim totalAmount = Convert.ToDouble(txtAmount.Text)
            Dim doubleTotal = Math.Round(totalAmount, 2)
            Dim finalAmount = doubleTotal.ToString("N", CultureInfo.InvariantCulture)

            Dim tendered = Convert.ToDouble(txtTendered.Text)
            Dim doubleTendered = Math.Round(tendered, 2)
            Dim finalTendered = doubleTendered.ToString("N", CultureInfo.InvariantCulture)

            Dim change = tendered - totalAmount

            Dim totalArrears = Convert.ToDouble(GlobalVariables.TotalArrears)
            Dim doubleArrears = Math.Round(totalArrears, 2)
            Dim finalArrears = doubleArrears.ToString("N", CultureInfo.InvariantCulture)

            Dim currentBill = Convert.ToDouble(GlobalVariables.CurBillAmount)
            Dim doubleCurrent = Math.Round(currentBill, 2)
            Dim finalCurrent = doubleCurrent.ToString("N", CultureInfo.InvariantCulture)

            Dim remainingBal = totalArrears - tendered

            If change < 0 Then
                change = 0.00
                txtAmount.Text = finalTendered
                txtAmount.Update()
                totalAmount = Convert.ToDouble(txtAmount.Text)
                doubleTotal = Math.Round(totalAmount, 2)
                finalAmount = doubleTotal.ToString("N", CultureInfo.InvariantCulture)
            Else
                remainingBal = totalArrears - totalAmount
            End If

            Dim doubleChange = Math.Round(change, 2)
            Dim finalChange = doubleChange.ToString("N", CultureInfo.InvariantCulture)

            Dim doubleBal = Math.Round(remainingBal, 2)
            Dim finalRemainingBal = doubleBal.ToString("N", CultureInfo.InvariantCulture)

            Dim payingBill = rdbCurrentOnly.Text
            Dim payingBalance As String = GlobalVariables.CurBillAmount
            Dim payingSurcharge As String = GlobalVariables.CurBillSurcharge
            Dim payingVat As String = GlobalVariables.CurBillVat
            Dim payingTotal As String = GlobalVariables.CurBillTotal
            If rdbCurrentOnly.Checked = True Then
                payingBill = rdbCurrentOnly.Text
                payingBalance = GlobalVariables.CurBillAmount
                payingSurcharge = "0.00"
                payingVat = "0.00"
                payingTotal = GlobalVariables.CurBillAmount
            ElseIf rdbCurrent.Checked = True Then
                payingBill = rdbCurrent.Text
                payingBalance = GlobalVariables.CurBillAmount
                payingSurcharge = GlobalVariables.CurBillSurcharge
                payingVat = GlobalVariables.CurBillVat
                payingTotal = GlobalVariables.CurBillTotal
            ElseIf rdbOver30.Checked = True Then
                payingBill = rdbOver30.Text
                payingBalance = GlobalVariables.Over30Amount
                payingSurcharge = GlobalVariables.Over30Surcharge
                payingVat = GlobalVariables.Over30Vat
                payingTotal = GlobalVariables.Over30Total
            ElseIf rdbOver60.Checked = True Then
                payingBill = rdbOver60.Text
                payingBalance = GlobalVariables.Over60Amount
                payingSurcharge = GlobalVariables.Over60Surcharge
                payingVat = GlobalVariables.Over60Vat
                payingTotal = GlobalVariables.Over60Total
            ElseIf rdbOver90.Checked = True Then
                payingBill = rdbOver90.Text
                payingBalance = GlobalVariables.Over90Amount
                payingSurcharge = GlobalVariables.Over90Surcharge
                payingVat = GlobalVariables.Over90Vat
                payingTotal = GlobalVariables.Over90Total
            ElseIf rdbOver120.Checked = True Then
                payingBill = rdbOver120.Text
                payingBalance = GlobalVariables.Over120Amount
                payingSurcharge = GlobalVariables.Over120Surcharge
                payingVat = GlobalVariables.Over120Vat
                payingTotal = GlobalVariables.Over120Total
            End If


            Dim msg = "CONFIRM PAYMENT DETAILS" + vbNewLine +
                          "----------------------------------------" + vbNewLine +
                          "Account: " + txtAccount.Text + vbNewLine +
                          "Account Name: " + txtName.Text + vbNewLine +
                          "Mobile: " + txtMobile.Text + vbNewLine +
                          "Statement of Account: " + GlobalVariables.StatemenAccount + vbNewLine +
                          "Meter Number: " + txtMeterNo.Text + vbNewLine +
                          "OR Number: " + txtORNumber.Text + vbNewLine +
                          "Balance: Php" + lblBalance.Text + vbNewLine +
                          "----------------------------------------" + vbNewLine +
                          "Total Arrears Overdue: " + finalArrears + vbNewLine +
                          "Paying Bill: " + payingBill + vbNewLine +
                          payingBill + " Balance: " + payingBalance + vbNewLine +
                          payingBill + " Surcharge: " + payingSurcharge + vbNewLine +
                          payingBill + " VAT: " + payingVat + vbNewLine +
                          payingBill + " Total: " + payingTotal + vbNewLine +
                          "----------------------------------------" + vbNewLine +
                          "PAYING AMOUNT: Php" + finalAmount + vbNewLine +
                          "CASH TENDERED: Php" + finalTendered + vbNewLine +
                          "CHANGE: Php" + finalChange + vbNewLine +
                          "----------------------------------------" + vbNewLine

            Dim msgResult As Integer = MessageBox.Show(msg, "Payment Validation", MessageBoxButtons.OKCancel)
            If msgResult = DialogResult.Cancel Then

            ElseIf msgResult = DialogResult.OK Then
                ProcessPayment()
            End If
        End If

    End Sub

    Private Sub PrintDocument1_PrintPage(sender As Object, e As Printing.PrintPageEventArgs) _
       Handles PrintDocument1.PrintPage
        '' Do something e.g. 
        'e.Graphics.DrawString("test page #1", New Font("Arial", 12, FontStyle.Bold), Brushes.Black, 10, 10)
        'e.HasMorePages = False

        Static currentChar As Integer

        Dim textfont As Font = New Font("Arial", 8, FontStyle.Regular)

        Dim h, w As Integer
        Dim left, top As Integer
        With PrintDocument1.DefaultPageSettings
            h = 0
            w = 0
            left = 0
            top = 0
        End With


        Dim lines As Integer = CInt(Math.Round(h / 2))
        Dim b As New Rectangle(left, top, w, h)
        Dim format As StringFormat
        format = New StringFormat(StringFormatFlags.LineLimit)
        Dim line, chars As Integer


        e.Graphics.MeasureString(Mid(TextToPrint, currentChar + 1), textfont, New SizeF(w, h), format, chars, line)
        e.Graphics.DrawString(TextToPrint.Substring(currentChar, chars), New Font("Consolas", 9, FontStyle.Regular), Brushes.Black, b, format)


        currentChar = currentChar + chars
        If currentChar < TextToPrint.Length Then
            e.HasMorePages = True
        Else
            e.HasMorePages = False
            currentChar = 0
        End If
    End Sub

    Public Async Sub ProcessPayment()
        Dim res = Await ProcessPaymentTask()

        If String.IsNullOrEmpty(res) Then
            MsgBox("Connection Timeout.")
        Else

            Try
                Dim doc As XmlDocument = New XmlDocument()
                doc.LoadXml(res)

                Dim resCode = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseCode").InnerText()
                Dim resMsg = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseMessage").InnerText()

                If String.Equals(resCode, "0000") Then
                    Dim resTransId = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/TransactionId").InnerText()
                    Dim resName = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/CustomerName").InnerText()
                    Dim resMobile = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/MobileNumber").InnerText()
                    Dim resAmount = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/Amount").InnerText()
                    Dim resSOA = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/StatementOfAccountNumber").InnerText()
                    Dim resAccount = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/AccountNumber").InnerText()
                    Dim resCompany = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/CompanyId").InnerText()
                    Dim resComUser = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/CompanyUsername").InnerText()
                    Dim resDate = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/DateTimeStamp").InnerText()
                    Dim resStatus = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/Status").InnerText()
                    Dim resBalLimit = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/BalanceLimit").InnerText()
                    Dim resService = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/ServiceCharge").InnerText()
                    'Dim resSurcharge = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/SurCharge").InnerText()
                    'Dim resAddress = txtAddress.Text
                    'Dim resMeterNo = txtMeterNo.Text
                    Dim resAddress = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/Address").InnerText()
                    Dim resMeterNo = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/MeterNo").InnerText()
                    Dim resTotal = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/TotalAmount").InnerText()

                    MsgBox("Payment Successful to " + resName + vbNewLine +
                           "on " + resDate + vbNewLine)

                    Dim resBalance = doc.SelectSingleNode("/F8MobileXMLRequest/CurrentBalance").InnerText()
                    Dim balance = Convert.ToDouble(resBalance)
                    Dim doubleBalance = Math.Round(balance, 2)
                    Dim finalBal = doubleBalance.ToString("N", CultureInfo.InvariantCulture)

                    lblBalance.Text = "Balance: Php" + finalBal

                    txtAccount.Text = ""
                    txtName.Text = ""
                    txtMobile.Text = ""
                    txtAmount.Text = ""
                    txtIssuance.Text = ""
                    txtBillingMonth.Text = ""
                    txtTendered.Text = ""
                    txtAddress.Text = ""
                    txtMeterNo.Text = ""
                    txtORNumber.Text = ""

                    '**************** PRINT *****************
                    Dim selectedItem = cboPrinter.SelectedItem
                    ' Select the printer.
                    PrintDocument1.PrinterSettings.PrinterName = selectedItem
                    'PrintHeader()
                    'ItemsToBePrinted()
                    'printFooter()

                    Dim amount = Convert.ToDouble(resAmount)
                    Dim doubleAmount = Math.Round(amount, 2)
                    Dim finalAmount = doubleAmount.ToString("N", CultureInfo.InvariantCulture)

                    Dim conFee = Convert.ToDouble(resService)
                    Dim doubleConFee = Math.Round(conFee, 2)
                    Dim finalFee = doubleConFee.ToString("N", CultureInfo.InvariantCulture)

                    'Dim surcharge = Convert.ToDouble(resSurcharge)
                    'Dim doubleSurcharge = Math.Round(surcharge, 2)
                    'Dim finalSurcharge = doubleSurcharge.ToString("N", CultureInfo.InvariantCulture)

                    Dim totalArrears = Convert.ToDouble(GlobalVariables.TotalArrears)
                    Dim remArrears = totalArrears - amount
                    Dim doubleTotalArrears = Math.Round(remArrears, 2)
                    Dim finalRemaining = doubleTotalArrears.ToString("N", CultureInfo.InvariantCulture)

                    Dim payingBill As String = ""
                    Dim payingAmount As String = ""
                    Dim payingDate As String = ""
                    Dim payingSurcharge = ""
                    Dim payingVat = ""
                    Dim payingTotal = ""
                    If rdbCurrentOnly.Checked = True Then
                        payingAmount = GlobalVariables.CurBillAmount
                        payingDate = GlobalVariables.CurBillDate
                        payingBill = "Current"
                        payingSurcharge = "0.00"
                        payingVat = "0.00"
                        payingTotal = GlobalVariables.CurBillAmount
                    ElseIf rdbCurrent.Checked = True Then
                        payingAmount = GlobalVariables.CurBillAmount
                        payingDate = GlobalVariables.CurBillDate
                        payingBill = "Current"
                        payingSurcharge = GlobalVariables.CurBillSurcharge
                        payingVat = GlobalVariables.CurBillVat
                        payingTotal = GlobalVariables.CurBillTotal
                    ElseIf rdbOver30.Checked = True Then
                        payingAmount = GlobalVariables.Over30Amount
                        payingDate = GlobalVariables.Over30Date
                        payingBill = rdbOver30.Text
                        payingSurcharge = GlobalVariables.Over30Surcharge
                        payingVat = GlobalVariables.Over30Vat
                        payingTotal = GlobalVariables.Over30Total
                    ElseIf rdbOver60.Checked = True Then
                        payingAmount = GlobalVariables.Over60Amount
                        payingDate = GlobalVariables.Over60Date
                        payingBill = rdbOver60.Text
                        payingSurcharge = GlobalVariables.Over60Surcharge
                        payingVat = GlobalVariables.Over60Vat
                        payingTotal = GlobalVariables.Over60Total
                    ElseIf rdbOver90.Checked = True Then
                        payingAmount = GlobalVariables.Over90Amount
                        payingDate = GlobalVariables.Over90Date
                        payingBill = rdbOver90.Text
                        payingSurcharge = GlobalVariables.Over90Surcharge
                        payingVat = GlobalVariables.Over90Vat
                        payingTotal = GlobalVariables.Over90Total
                    ElseIf rdbOver120.Checked = True Then
                        payingAmount = GlobalVariables.Over120Amount
                        payingDate = GlobalVariables.Over1200Date
                        payingBill = rdbOver120.Text
                        payingSurcharge = GlobalVariables.Over120Surcharge
                        payingVat = GlobalVariables.Over120Vat
                        payingTotal = GlobalVariables.Over120Total
                    End If

                    'Dim vat = Convert.ToDouble(payingVat)

                    Dim total = Convert.ToDouble(amount)
                    Dim doubleTotal = Math.Round(total, 2)
                    Dim finalTotal = doubleTotal.ToString("N", CultureInfo.InvariantCulture)

                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    SetInfoToPrint(resName)
                    SetInfoToPrint(resAccount)
                    SetInfoToPrint(resAddress)
                    SetInfoToPrint(resMeterNo)
                    'ItemsToBePrinted("Amount:           ", "Php" + finalAmount)
                    'ItemsToBePrinted("Surcharge:        ", "Php" + finalSurcharge)
                    'ItemsToBePrinted("Convenienve Fee:  ", "Php" + finalFee)
                    'ItemsToBePrinted("Billing Month:    ", resSOA)
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line

                    SetParticularHeaders("---------BILLING DETAILS----------")
                    SetDetailsToPrint("Paying Bill: ", payingBill + " Bill")
                    SetDetailsToPrint(payingBill + "(" + payingDate + "):", payingAmount)
                    SetDetailsToPrint("Surcharge: ", payingSurcharge)
                    SetDetailsToPrint("Vat: ", payingVat)
                    SetDetailsToPrint("            ", "------------") ' New line
                    SetDetailsToPrint(payingBill + " Total: ", payingTotal)
                    SetDetailsToPrint("            ", "------------") ' New line

                    ItemsToBePrinted("", "") ' New line

                    SetParticularHeaders("---------PAYMENT DETAILS----------")
                    SetDetailsToPrint("Amount Paid: ", finalTotal)
                    SetDetailsToPrint("            ", "------------") ' New line

                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line

                    'SetParticularHeaders("---------OVERDUE ACCOUNTS---------")
                    'SetDetailsToPrint("Ovr30   (" + GlobalVariables.Over30Date + ")", GlobalVariables.Over30Total)
                    'SetDetailsToPrint("Ovr60   (" + GlobalVariables.Over60Date + ")", GlobalVariables.Over60Total)
                    'SetDetailsToPrint("Ovr90   (" + GlobalVariables.Over90Date + ")", GlobalVariables.Over90Total)
                    'SetDetailsToPrint("Ovr120  (" + GlobalVariables.Over1200Date + ")", GlobalVariables.Over120Total)
                    'SetDetailsToPrint("            ", "------------") ' New line
                    'SetDetailsToPrint("Total Arrears: ", GlobalVariables.TotalArrears)
                    'SetDetailsToPrint("            ", "------------") ' New line

                    ItemsToBePrinted("", "") ' New line

                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line

                    ItemsToBePrinted("", "") ' New line
                    ItemsToBePrinted("", "") ' New line
                    SetInfoToPrintSpace(finalTotal)
                    SetInfoToPrintSpace("** CASH **   BRANCH: " + My.Settings.Branch)
                    ItemsToBePrinted("", "") ' New line

                    SetInfoToPrint("[" + fmLogin.txtUsername.Text + "] " + My.Settings.CashierName)

                    SetInfoToPrint("                " + resDate)

                    'ItemsToBePrinted("", "") ' New line
                    'send Business Name
                    'Dim StringToPrint As String = "---------------------------------"
                    'Dim LineLen As Integer = StringToPrint.Length
                    'Dim spcLen1 As New String(" "c, Math.Round((52 - LineLen) / 2)) 'This line is used to center text in the middle of the receipt
                    'TextToPrint &= spcLen1 & StringToPrint & Environment.NewLine
                    'ItemsToBePrinted("", "") ' New line

                    'ItemsToBePrinted("Total:            ", "Php" + "1000")

                    Dim printControl = New Printing.StandardPrintController
                    PrintDocument1.PrintController = printControl
                    Try
                        PrintDocument1.Print()
                        TextToPrint = ""
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try

                    'API CALL'
                    'Dim webClient As New System.Net.WebClient
                    'Dim result As String = WebClient.DownloadString("https://f8mobile.net/web_api/index.php/webservice_api/validate?mac_address=88888888")
                    'MessageBox.Show(result)

                Else
                    MsgBox("Transaction Failed." + vbNewLine + resMsg)
                End If
            Catch
                MsgBox("Connection Timeout.")
            End Try

        End If
    End Sub

    Public Async Sub ValidatePayment()
        Dim res = Await ValidatePaymentTask()

        If String.IsNullOrEmpty(res) Then
            MsgBox("Connection Timeout.")
        Else
            Dim doc As XmlDocument = New XmlDocument()
            doc.LoadXml(res)

            Dim resCode = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseCode").InnerText()
            Dim resMsg = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseMessage").InnerText()

            'Dim resCode As String = jsonRes("ResponseCode")
            ''MessageBox.Show(resCode)

            If String.Equals(resCode, "0000") Then
                Dim resAccount = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/AccountNumber").InnerText()
                Dim resName = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/CustomerName").InnerText()
                Dim resMobile = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/MobileNumber").InnerText()
                Dim resAmount = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/Amount").InnerText()
                Dim resFee = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/ServiceCharge").InnerText()
                Dim resSurcharge = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/SurCharge").InnerText()
                Dim resStatement = doc.SelectSingleNode("/F8MobileXMLRequest/ResponseData/StatementOfAccountNumber").InnerText()
                Dim resBalance = doc.SelectSingleNode("/F8MobileXMLRequest/BalanceNotification").InnerText()

                Dim amount = Convert.ToDouble(resAmount)
                Dim doubleAmount = Math.Round(amount, 2)
                Dim finalAmount = doubleAmount.ToString("N", CultureInfo.InvariantCulture)

                Dim conFee = Convert.ToDouble(resFee)
                Dim doubleConFee = Math.Round(conFee, 2)
                Dim finalFee = doubleConFee.ToString("N", CultureInfo.InvariantCulture)

                Dim surcharge = Convert.ToDouble(resSurcharge)
                Dim doubleSurcharge = Math.Round(surcharge, 2)
                Dim finalSurcharge = doubleSurcharge.ToString("N", CultureInfo.InvariantCulture)

                Dim balance = Convert.ToDouble(resBalance)
                Dim doubleBalance = Math.Round(balance, 2)
                Dim finalBalance = doubleBalance.ToString("N", CultureInfo.InvariantCulture)

                Dim totalAmount = amount + conFee + surcharge
                Dim doubleTotal = Math.Round(totalAmount, 2)
                Dim finalTotal = doubleTotal.ToString("N", CultureInfo.InvariantCulture)

                Dim tendered = Convert.ToDouble(txtTendered.Text)
                Dim doubleTendered = Math.Round(tendered, 2)
                Dim finalTendered = doubleTendered.ToString("N", CultureInfo.InvariantCulture)

                Dim change = Convert.ToDouble(txtTendered.Text) - amount
                Dim doubleChange = Math.Round(change, 2)
                Dim finalChange = doubleChange.ToString("N", CultureInfo.InvariantCulture)

                If (totalAmount > tendered) Then
                    MsgBox("Invalid Cash Tendered, Please check and try again.")
                Else
                    Dim msg = "CONFIRM PAYMENT DETAILS" + vbNewLine +
                       "----------------------------------------" + vbNewLine +
                       "Account: " + resAccount + vbNewLine +
                       "Account Name: " + resName + vbNewLine +
                       "Mobile: " + resMobile + vbNewLine +
                       "Amount: Php" + finalAmount + vbNewLine +
                       "Statement of Account: " + resStatement + vbNewLine +
                       "Balance: Php" + finalBalance + vbNewLine +
                       "----------------------------------------" + vbNewLine +
                       "TOTAL AMOUNT: Php" + finalAmount + vbNewLine +
                       "CASH TENDERED: Php" + finalTendered + vbNewLine +
                       "CHANGE: Php" + finalChange + vbNewLine
                    Dim msgResult As Integer = MessageBox.Show(msg, "Payment Validation", MessageBoxButtons.OKCancel)
                    If msgResult = DialogResult.Cancel Then

                    ElseIf msgResult = DialogResult.OK Then
                        ProcessPayment()
                    End If
                End If
            Else
                MsgBox("Failed to Validate." + vbNewLine + resMsg)
            End If
        End If

    End Sub

    Public Sub GetBillDetails()
        'Dim res As String = Await GetBillDetailsTask()
        Debug.WriteLine("Account: " + txtAccount.Text)

        Dim res As String = postData()
        'MsgBox(res)

        If String.IsNullOrEmpty(res) Then
            MsgBox("Connection Timeout!")
            txtAccount.Text = ""
            txtAccount.Refresh()
            txtAccount.Focus()
        Else
            Dim jsonRes As JObject = JObject.Parse(res)
            Dim data As List(Of JToken) = jsonRes.Children().ToList
            Dim output As String = ""

            Dim resCode As String = jsonRes("ResponseCode")
            'MessageBox.Show(resCode)

            If String.Equals(resCode, "0000") Then
                Dim resDataArray As JArray = jsonRes("ResponseData")
                Dim resData As JObject = resDataArray(0)
                'MsgBox(resData.ToString)
                Dim resCustId As String = resData("CustomerId")
                Dim resNewAccount As String = resData("Newacts")
                Dim resCustType As String = resData("Ctype")
                Dim resCustName As String = resData("Cname")
                Dim resCustAddress As String = resData("Caddress")
                Dim resBillSat As String = resData("Billsat")
                Dim resNewMsn As String = resData("Newmsn")
                Dim resDateInst As String = resData("Dateinst")
                Dim resDateDisco As String = resData("Datedisco")
                Dim resDateReco As String = resData("Datereco")
                Dim resSrFlag As String = resData("Srflag")
                Dim resBillMonth As String = resData("Billmonth")
                Dim resBillYear As String = resData("Billyear")
                Dim resDueDate As String = resData("Duedate")

                Dim resPrsmtrrdng As String = resData("Prsmtrrdng")
                Dim resBillDate As String = resData("Curbildat")
                Dim resCurMonKw As String = resData("Curmonkwhu")
                Dim resCurBillNo As String = resData("Currbillno")

                Dim resCurBill As String = decimalStringChecker(resData("Currentbil"))
                Dim resCurBillDate As String = resData("Bmycurr")
                Dim resCurBillSur As String = decimalStringChecker(resData("CurrSurch"))
                Dim resCurBillVat As String = decimalStringChecker(resData("CurrVat"))

                Dim resOver30 As String = resData("Over30day")
                Dim resOver30date As String = resData("Bmyov30")
                Dim resOver30Sur As String = resData("Ov30Surch")
                Dim resOver30Vat As String = resData("Ov30Vat")

                Dim resOver60 As String = resData("Over60day")
                Dim resOver60Date As String = resData("Bmyov60")
                Dim resOver60Sur As String = resData("Ov60Surch")
                Dim resOver60Vat As String = resData("Ov60Vat")

                Dim resOver90 As String = resData("Over90day")
                Dim resOver90Date As String = resData("Bmyov90")
                Dim resOver90Sur As String = resData("Ov90Surch")
                Dim resOver90Vat As String = resData("Ov90Vat")

                Dim resOver120 As String = resData("Over120day")
                Dim resOver120Date As String = resData("Bmyov120")
                Dim resOver120Sur As String = resData("Ov120Surch")
                Dim resOver120Vat As String = resData("Ov120Vat")

                Dim resTotal As String = resData("Totalar")
                'Dim doubleBalance = Math.Round(resBalance, 2)
                '' Dim finalBal = doubleBalance.ToString("N", CultureInfo.InvariantCulture)


                Dim doubleCurrBill = Math.Round(Convert.ToDouble(resCurBill), 2)
                Dim doubleCurrSur = Math.Round(Convert.ToDouble(resCurBillSur), 2)
                Dim doubleCurrVat = Math.Round(Convert.ToDouble(resCurBillVat), 2)
                'Dim totalCurBillVat = doubleCurrBill + doubleCurrVat
                Dim totalCurBill = doubleCurrBill + doubleCurrSur + doubleCurrVat

                'MessageBox.Show(totalCurBill)

                Dim doubleOver30 = Math.Round(Convert.ToDouble(resOver30), 2)
                Dim doubleOver30Sur = Math.Round(Convert.ToDouble(resOver30Sur), 2)
                Dim doubleOver30Vat = Math.Round(Convert.ToDouble(resOver30Vat), 2)
                Dim totalOver30 = doubleOver30 + doubleOver30Sur + doubleOver30Vat

                Dim doubleOver60 = Math.Round(Convert.ToDouble(resOver60), 2)
                Dim doubleOver60Sur = Math.Round(Convert.ToDouble(resOver60Sur), 2)
                Dim doubleOver60Vat = Math.Round(Convert.ToDouble(resOver60Vat), 2)
                Dim totalOver60 = doubleOver60 + doubleOver60Sur + doubleOver60Vat

                Dim doubleOver90 = Math.Round(Convert.ToDouble(resOver90), 2)
                Dim doubleOver90Sur = Math.Round(Convert.ToDouble(resOver90Sur), 2)
                Dim doubleOver90Vat = Math.Round(Convert.ToDouble(resOver90Vat), 2)
                Dim totalOver90 = doubleOver90 + doubleOver90Sur + doubleOver90Vat

                Dim doubleOver120 = Math.Round(Convert.ToDouble(resOver120), 2)
                Dim doubleOver120Sur = Math.Round(Convert.ToDouble(resOver120Sur), 2)
                Dim doubleOver120Vat = Math.Round(Convert.ToDouble(resOver120Vat), 2)
                Dim totalOver120 = doubleOver120 + doubleOver120Sur + doubleOver120Vat

                Dim doubleTotalArrears = Math.Round(Convert.ToDouble(resTotal), 2)
                Dim finalTotal = doubleTotalArrears.ToString("N", CultureInfo.InvariantCulture)

                Dim total =
                    totalOver30 +
                    totalOver60 +
                    totalOver90 +
                    totalOver120

                Dim finalTotalArrears = total.ToString("N", CultureInfo.InvariantCulture)
                Dim totalAmountArrears = doubleTotalArrears.ToString("N", CultureInfo.InvariantCulture)
                'txtAmount.Text = finalTotal
                'txtAmount.Update()

                txtName.Text = resCustName
                txtName.Update()

                txtBillingMonth.Text = resBillYear + resBillMonth
                txtBillingMonth.Update()

                txtAddress.Text = resCustAddress
                txtAddress.Update()

                txtMeterNo.Text = resNewMsn
                txtMeterNo.Update()

                '' SET GLOBAL VARIABLE ''
                GlobalVariables.CurBillAmount = doubleCurrBill.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.CurBillSurcharge = doubleCurrSur.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.CurBillVat = doubleCurrVat.ToString("N", CultureInfo.InvariantCulture)
                'GlobalVariables.CurBillTotalVat = totalCurBillVat.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.CurBillTotal = totalCurBill.ToString("N", CultureInfo.InvariantCulture)

                GlobalVariables.Over30Amount = doubleOver30.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over30Surcharge = doubleOver30Sur.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over30Vat = doubleOver30Vat.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over30Total = totalOver30.ToString("N", CultureInfo.InvariantCulture)

                GlobalVariables.Over60Amount = doubleOver60.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over60Surcharge = doubleOver60Sur.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over60Vat = doubleOver60Vat.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over60Total = totalOver60.ToString("N", CultureInfo.InvariantCulture)

                GlobalVariables.Over90Amount = doubleOver90.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over90Surcharge = doubleOver90Sur.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over90Vat = doubleOver90Vat.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over90Total = totalOver90.ToString("N", CultureInfo.InvariantCulture)

                GlobalVariables.Over120Amount = doubleOver120.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over120Surcharge = doubleOver120Sur.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over120Vat = doubleOver120Vat.ToString("N", CultureInfo.InvariantCulture)
                GlobalVariables.Over120Total = totalOver120.ToString("N", CultureInfo.InvariantCulture)

                GlobalVariables.TotalArrears = finalTotalArrears

                GlobalVariables.CurBillDate = resCurBillDate
                GlobalVariables.Over30Date = resOver30date
                GlobalVariables.Over60Date = resOver60Date
                GlobalVariables.Over90Date = resOver90Date
                GlobalVariables.Over1200Date = resOver120Date

                GlobalVariables.StatemenAccount = resBillDate

                Dim final_res As String = "Account Validated!" + vbNewLine + vbNewLine +
                    "Name : " + resCustName + vbNewLine +
                    "Account # : " + resNewAccount + vbNewLine +
                    "Current Only (" + resCurBillDate + ") : Php" + GlobalVariables.CurBillAmount + vbNewLine +
                    "Over30 : " + GlobalVariables.Over30Total + vbNewLine +
                    "Over60 : " + GlobalVariables.Over60Total + vbNewLine +
                    "Over90 : " + GlobalVariables.Over90Total + vbNewLine +
                    "Over120 : " + GlobalVariables.Over120Total + vbNewLine + vbNewLine +
                    "Address : " + resCustAddress + vbNewLine +
                    "Meter # : " + resNewMsn + vbNewLine

                rdbCurrentOnly.Checked = True
                rdbCurrentOnly.Update()


                MsgBox(final_res)
            Else
                MsgBox("Invalid Account! No records found.")
                txtAccount.Text = ""
                txtAccount.Refresh()

                txtName.Text = ""
                txtName.Refresh()

                txtMobile.Text = ""
                txtMobile.Refresh()

                txtAmount.Text = ""
                txtAmount.Refresh()

                txtIssuance.Text = ""
                txtIssuance.Refresh()

                txtBillingMonth.Text = ""
                txtBillingMonth.Refresh()

                txtTendered.Text = ""
                txtTendered.Refresh()

                txtAddress.Text = ""
                txtAddress.Refresh()

                txtMeterNo.Text = ""
                txtMeterNo.Refresh()

                txtAccount.Focus()

                GlobalVariables.CurBillAmount = "0.0"
                GlobalVariables.Over30Amount = "0.0"
                GlobalVariables.Over60Amount = "0.0"
                GlobalVariables.Over90Amount = "0.0"
                GlobalVariables.Over120Amount = "0.0"
                GlobalVariables.TotalArrears = "0.0"

                GlobalVariables.CurBillDate = ""
                GlobalVariables.Over30Date = ""
                GlobalVariables.Over60Date = ""
                GlobalVariables.Over90Date = ""
                GlobalVariables.Over1200Date = ""

                GlobalVariables.StatemenAccount = ""
            End If
        End If
    End Sub

    Public Async Sub GetBalance()
        Dim res As String = Await AccessTheWebAsync()

        If String.IsNullOrEmpty(res) Then
            lblBalance.Text = "Balance: Php0.00"
        Else
            Dim jsonRes As JObject = JObject.Parse(res)
            'Dim data As List(Of JToken) = ser.Children().ToList
            Dim output As String = ""

            Dim resCode As String = jsonRes("ResponseCode")
            'MessageBox.Show(resCode)

            If String.Equals(resCode, "0000") Then
                Dim resData As JObject = jsonRes("ResponseData")
                Dim resBalance = Convert.ToDouble(resData("ClientBalance"))
                Dim doubleBalance = Math.Round(resBalance, 2)
                Dim finalBal = doubleBalance.ToString("N", CultureInfo.InvariantCulture)
                'MessageBox.Show(resBalance)




                lblBalance.Text = "Balance: Php" + finalBal
            Else
                MsgBox("Failed to get Client balance," + vbNewLine + "Kindly refresh to check balance.")
            End If
        End If
    End Sub

    Async Function ProcessPaymentTask() As Task(Of String)

        Dim payingSurcharge As String = GlobalVariables.CurBillSurcharge
        Dim payingVat As String = GlobalVariables.CurBillVat
        If rdbCurrentOnly.Checked = True Then
            payingSurcharge = "0.00"
            payingVat = "0.00"
        ElseIf rdbCurrent.Checked = True Then
            payingSurcharge = GlobalVariables.CurBillSurcharge
            payingVat = GlobalVariables.CurBillVat
        ElseIf rdbOver30.Checked = True Then
            payingSurcharge = GlobalVariables.Over30Surcharge
            payingVat = GlobalVariables.Over30Vat
        ElseIf rdbOver60.Checked = True Then
            payingSurcharge = GlobalVariables.Over60Surcharge
            payingVat = GlobalVariables.Over60Vat
        ElseIf rdbOver90.Checked = True Then
            payingSurcharge = GlobalVariables.Over90Surcharge
            payingVat = GlobalVariables.Over90Vat
        ElseIf rdbOver120.Checked = True Then
            payingSurcharge = GlobalVariables.Over120Surcharge
            payingVat = GlobalVariables.Over120Vat
        End If

        Dim urlUser = WebUtility.UrlEncode(fmLogin.txtUsername.Text)
        Dim urlAccount = WebUtility.UrlEncode(txtAccount.Text)
        Dim urlName = WebUtility.UrlEncode(txtName.Text)
        Dim urlMobile = WebUtility.UrlEncode(txtMobile.Text)
        Dim urlAmount = WebUtility.UrlEncode(txtAmount.Text)
        Dim urlIssuance = WebUtility.UrlEncode(txtIssuance.Text)
        Dim urlBilling = WebUtility.UrlEncode(txtBillingMonth.Text)
        Dim urlAddress = WebUtility.UrlEncode(txtAddress.Text)
        Dim urlMeterNo = WebUtility.UrlEncode(txtMeterNo.Text)
        Dim urlORNumber = WebUtility.UrlEncode(txtORNumber.Text)
        Dim urlSurcharge = WebUtility.UrlEncode(payingSurcharge)
        Dim urlVat = WebUtility.UrlEncode(payingVat)

        'Dim url = "https://f8mobile.net/portal/e8wallet_api/encode?account=" + urlAccount + "&name=" + urlName + "&username=" + urlUser + "&mobile=" + urlMobile + "&amount=" + urlAmount + "&statement_number=" + urlBilling + "&company=2&issuance_date=" + urlIssuance + "&type=mobile&address=" + urlAddress + "&meter_no=" + urlMeterNo + "&or_no=" + urlORNumber
        Dim url = "https://f8mobile.net/portal/e8wallet_api/encode_arrears?account=" + urlAccount + "&name=" + urlName + "&username=" + urlUser + "&mobile=" + urlMobile + "&amount=" + urlAmount + "&statement_number=" + urlBilling + "&company=2&issuance_date=" + urlIssuance + "&type=mobile&address=" + urlAddress + "&meter_no=" + urlMeterNo + "&service_charge=0&surcharge=" + urlSurcharge + "&vat=" + urlVat + "&or_no=" + urlORNumber

        Debug.WriteLine("URL: " + url)

        Try
            ' You need to add a reference to System.Net.Http to declare client.  
            Dim client As HttpClient = New HttpClient()

            ' GetStringAsync returns a Task(Of String). That means that when you await the  
            ' task you'll get a string (urlContents).  
            Dim getStringTask As Task(Of String) = client.GetStringAsync(url)

            ' You can do work here that doesn't rely on the string from GetStringAsync.  
            'DoIndependentWork()

            ' The Await operator suspends AccessTheWebAsync.  
            '  - AccessTheWebAsync can't continue until getStringTask is complete.  
            '  - Meanwhile, control returns to the caller of AccessTheWebAsync.  
            '  - Control resumes here when getStringTask is complete.   
            '  - The Await operator then retrieves the string result from getStringTask.  
            Dim urlContents As String = Await getStringTask

            ' The return statement specifies an integer result.  
            ' Any methods that are awaiting AccessTheWebAsync retrieve the length value.  
            Return urlContents
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Async Function ValidatePaymentTask() As Task(Of String)

        'Dim urlContents As String = ""

        Dim urlUser = WebUtility.UrlEncode(fmLogin.txtUsername.Text)
        Dim urlAccount = WebUtility.UrlEncode(txtAccount.Text)
        Dim urlName = WebUtility.UrlEncode(txtName.Text)
        Dim urlMobile = WebUtility.UrlEncode(txtMobile.Text)
        Dim urlAmount = WebUtility.UrlEncode(txtAmount.Text)
        Dim urlIssuance = WebUtility.UrlEncode(txtIssuance.Text)
        Dim urlBilling = WebUtility.UrlEncode(txtBillingMonth.Text)

        Try
            ' You need to add a reference to System.Net.Http to declare client.  
            Dim client As HttpClient = New HttpClient()

            ' GetStringAsync returns a Task(Of String). That means that when you await the  
            ' task you'll get a string (urlContents).  
            Dim getStringTask As Task(Of String) = client.GetStringAsync("https://f8mobile.net/portal/e8wallet_api/validate_issuance_date?account=" + urlAccount + "&name=" + urlName + "&username=" + urlUser + "&mobile=" + urlMobile + "&amount=" + urlAmount + "&statement_number=" + urlBilling + "&company=2&issuance_date=" + urlIssuance + "&type=mobile")

            ' You can do work here that doesn't rely on the string from GetStringAsync.  
            'DoIndependentWork()

            ' The Await operator suspends AccessTheWebAsync.  
            '  - AccessTheWebAsync can't continue until getStringTask is complete.  
            '  - Meanwhile, control returns to the caller of AccessTheWebAsync.  
            '  - Control resumes here when getStringTask is complete.   
            '  - The Await operator then retrieves the string result from getStringTask.  
            Dim urlContents As String = Await getStringTask

            ' The return statement specifies an integer result.  
            ' Any methods that are awaiting AccessTheWebAsync retrieve the length value.  

            Return urlContents
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Function postData() As String
        Dim webClient As New WebClient()
        Dim resByte As Byte()
        Dim resString As String

        Dim reqparm As New Specialized.NameValueCollection
        reqparm.Add("method", "get_bill_details")
        reqparm.Add("P01", txtAccount.Text)

        Try
            webClient.Headers("Content-type") = "application/x-www-form-urlencoded"
            'reqString = Encoding.Default.GetBytes(FormUrlEncodedContent(dictData))
            'reqString =
            'Debug.WriteLine(reqString.ToString)
            resByte = webClient.UploadValues("https://f8mobile.net/ewallet_platform/services/pull_reports/", "post", reqparm)
            resString = Encoding.Default.GetString(resByte)
            Debug.WriteLine(resString)
            webClient.Dispose()
            Return resString
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
        End Try
        Return ""
    End Function

    Async Function AccessTheWebAsync() As Task(Of String)

        Try
            ' You need to add a reference to System.Net.Http to declare client.  
            Dim client As HttpClient = New HttpClient()

            ' GetStringAsync returns a Task(Of String). That means that when you await the  
            ' task you'll get a string (urlContents).  
            Dim getStringTask As Task(Of String) = client.GetStringAsync("https://f8mobile.net/webservices/api/balance?username=" + fmLogin.txtUsername.Text)

            ' You can do work here that doesn't rely on the string from GetStringAsync.  
            'DoIndependentWork()

            ' The Await operator suspends AccessTheWebAsync.  
            '  - AccessTheWebAsync can't continue until getStringTask is complete.  
            '  - Meanwhile, control returns to the caller of AccessTheWebAsync.  
            '  - Control resumes here when getStringTask is complete.   
            '  - The Await operator then retrieves the string result from getStringTask.  
            Dim urlContents As String = Await getStringTask

            ' The return statement specifies an integer result.  
            ' Any methods that are awaiting AccessTheWebAsync retrieve the length value.  
            Return urlContents
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Sub txtAccount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAccount.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
            MessageBox.Show("Please enter numbers only")
            e.Handled = True
        End If
    End Sub

    Private Sub txtMobile_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMobile.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
            MessageBox.Show("Please enter numbers only")
            e.Handled = True
        End If
    End Sub

    Private Sub txtAmount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmount.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not (Char.IsDigit(e.KeyChar) Or e.KeyChar = ".") Then
            MessageBox.Show("Please enter numbers only")
            e.Handled = True
        End If
    End Sub

    Private Sub txtTendered_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTendered.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not (Char.IsDigit(e.KeyChar) Or e.KeyChar = ".") Then
            MessageBox.Show("Please enter numbers only")
            e.Handled = True
        End If
    End Sub

    Private Sub txtBillingMonth_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtBillingMonth.KeyPress
        If Asc(e.KeyChar) <> 13 AndAlso Asc(e.KeyChar) <> 8 AndAlso Not IsNumeric(e.KeyChar) Then
            MessageBox.Show("Please enter numbers only")
            e.Handled = True
        End If
    End Sub

    Public Sub PrintHeader()

        TextToPrint = ""

        'send Business Name
        Dim StringToPrint As String = "F8 BAYAD EXPRESS"
        Dim LineLen As Integer = StringToPrint.Length
        Dim spcLen1 As New String(" "c, Math.Round((52 - LineLen) / 2)) 'This line is used to center text in the middle of the receipt
        TextToPrint &= spcLen1 & StringToPrint & Environment.NewLine

        'send address name
        StringToPrint = "12345 Street Avenue"
        LineLen = StringToPrint.Length
        Dim spcLen2 As New String(" "c, Math.Round((52 - LineLen) / 2))
        TextToPrint &= spcLen2 & StringToPrint & Environment.NewLine

        ' send city, state, zip
        StringToPrint = "BIGPROD PAYMENTS"
        LineLen = StringToPrint.Length
        Dim spcLen3 As New String(" "c, Math.Round((52 - LineLen) / 2))
        TextToPrint &= spcLen3 & StringToPrint & Environment.NewLine

        ' send phone number
        StringToPrint = "TIN# 279-212-870-001"
        LineLen = StringToPrint.Length
        Dim spcLen4 As New String(" "c, Math.Round((52 - LineLen) / 2))
        TextToPrint &= spcLen4 & StringToPrint & Environment.NewLine

        'send website
        StringToPrint = "www.bayadexpress.biz"
        LineLen = StringToPrint.Length
        Dim spcLen4b As New String(" "c, Math.Round((52 - LineLen) / 2))
        TextToPrint &= spcLen4b & StringToPrint & Environment.NewLine

        'send website
        StringToPrint = ""
        LineLen = StringToPrint.Length
        Dim spcLen5 As New String(" "c, Math.Round((52 - LineLen) / 2))
        TextToPrint &= spcLen5 & StringToPrint & Environment.NewLine

    End Sub

    Public Sub ItemsToBePrinted(prtDesc, prtValue)

        Dim LineLen As String = prtValue.Length
        Dim spcLen5 As New String(" "c, Math.Round((30 - LineLen)))

        While LineLen < 30
            prtValue = " " + prtValue
            LineLen += 1
        End While

        Dim StringToPrint As String = prtValue

        TextToPrint &= prtDesc & StringToPrint & Environment.NewLine
        'TextToPrint &= StringToPrint & Environment.NewLine

    End Sub

    Public Sub SetInfoToPrint(prtValue)

        Dim fValue As String = "               " + prtValue
        Dim LineLen As String = fValue.Length

        While LineLen < 75
            fValue = fValue + " "
            LineLen += 1
        End While

        TextToPrint &= fValue & prtValue & Environment.NewLine
        'TextToPrint &= StringToPrint & Environment.NewLine

    End Sub

    Public Sub SetInfoToPrintSpace(prtValue)

        Dim fValue As String = "         " + prtValue
        Dim LineLen As String = fValue.Length

        While LineLen < 69
            fValue = fValue + " "
            LineLen += 1
        End While

        TextToPrint &= fValue & prtValue & Environment.NewLine
        'TextToPrint &= StringToPrint & Environment.NewLine

    End Sub

    Public Sub SetParticularHeaders(prtValue)

        Dim fValue As String = "           " + prtValue
        Dim LineLen As String = fValue.Length

        While LineLen < 71
            fValue = fValue + " "
            LineLen += 1
        End While

        TextToPrint &= fValue & prtValue & Environment.NewLine
        'TextToPrint &= StringToPrint & Environment.NewLine

    End Sub

    Public Sub SetDetailsToPrint(prtValue, prtAmount)

        Dim fValue As String = "           " + prtValue
        Dim LineLen As String = fValue.Length

        While LineLen < 31
            fValue = fValue + " "
            LineLen += 1
        End While

        Dim LineLen1 As String = prtAmount.Length

        While LineLen1 < 21
            prtAmount = " " + prtAmount
            LineLen1 += 1
        End While

        Dim fReceipt = fValue & prtAmount

        Dim sValue As String = prtValue
        Dim LineLen3 As String = sValue.Length

        While LineLen3 < 20
            sValue = sValue + " "
            LineLen3 += 1
        End While

        Dim SReceipt As String = sValue & prtAmount
        Dim LineLen2 As String = fReceipt.Length

        While LineLen2 < 71
            fReceipt = fReceipt + " "
            LineLen2 += 1
        End While

        TextToPrint &= fReceipt & SReceipt & Environment.NewLine

    End Sub

    Public Sub printFooter()
        TextToPrint &= Environment.NewLine & Environment.NewLine
        Dim globalLengt As Integer = 0

        'SubTotal Amount
        Dim StringToPrint As String = "Sub Total   " & FormatCurrency("3.99", , , TriState.True, TriState.True)  'Change here to subtotal
        Dim LineLen As String = StringToPrint.Length
        globalLengt = StringToPrint.Length
        Dim spcLen5 As New String(" "c, Math.Round((26 - LineLen)))
        TextToPrint &= Environment.NewLine & spcLen5 & StringToPrint & Environment.NewLine

        'Tax Amount
        StringToPrint = "Tax         " & FormatCurrency("0.05", , , TriState.True, TriState.True) 'Change to tax amount
        LineLen = globalLengt
        Dim spcLen6 As New String(" "c, Math.Round((26 - LineLen)))
        If Not StringToPrint = "Tax         $0.00" Then
            TextToPrint &= spcLen6 & StringToPrint & Environment.NewLine
        End If

        'Total Amount
        StringToPrint = "Total       " & "$4.04"
        LineLen = globalLengt
        Dim spcLen8 As New String(" "c, Math.Round((26 - LineLen)))
        TextToPrint &= spcLen8 & StringToPrint & Environment.NewLine & Environment.NewLine

        'Cash Entered Amount
        StringToPrint = "Cash        " & FormatCurrency("5.00", , , TriState.True, TriState.True)
        LineLen = globalLengt
        Dim spcLen9 As New String(" "c, Math.Round((26 - LineLen)))
        If Not StringToPrint = "Cash        $0.00" Then
            TextToPrint &= spcLen9 & StringToPrint & Environment.NewLine
        End If

        'Change Amount
        StringToPrint = "Change      " & FormatCurrency("0.96", , , TriState.True, TriState.True)
        LineLen = globalLengt
        Dim spcLen10 As New String(" "c, Math.Round((26 - LineLen)))
        TextToPrint &= Environment.NewLine & spcLen10 & StringToPrint & Environment.NewLine
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        frmSettings.Show()
    End Sub


    Private Sub txtAccount_LostFocus(sender As Object, e As EventArgs) Handles txtAccount.LostFocus
        If String.IsNullOrEmpty(txtAccount.Text) Then '
            MsgBox("Please Enter Customer's Account Number") '
        Else
            GetBillDetails()
        End If
    End Sub

    Private Sub rdbCurrentOnly_CheckedChanged(sender As Object, e As EventArgs) Handles rdbCurrentOnly.CheckedChanged
        If rdbCurrentOnly.Checked = True Then
            txtAmount.Text = GlobalVariables.CurBillAmount
            txtAmount.Update()
        Else
            txtAmount.Text = "0.00"
            txtAmount.Update()
        End If
    End Sub

    Private Sub rdbOver30_CheckedChanged(sender As Object, e As EventArgs) Handles rdbOver30.CheckedChanged
        If rdbOver30.Checked = True Then
            txtAmount.Text = GlobalVariables.Over30Total
            txtAmount.Update()
        Else
            txtAmount.Text = "0.00"
            txtAmount.Update()
        End If
    End Sub

    Private Sub rdbOver60_CheckedChanged(sender As Object, e As EventArgs) Handles rdbOver60.CheckedChanged
        If rdbOver60.Checked = True Then
            txtAmount.Text = GlobalVariables.Over60Total
            txtAmount.Update()
        Else
            txtAmount.Text = "0.00"
            txtAmount.Update()
        End If
    End Sub

    Private Sub rdbOver90_CheckedChanged(sender As Object, e As EventArgs) Handles rdbOver90.CheckedChanged
        If rdbOver90.Checked = True Then
            txtAmount.Text = GlobalVariables.Over90Total
            txtAmount.Update()
        Else
            txtAmount.Text = "0.00"
            txtAmount.Update()
        End If
    End Sub

    Private Sub rdbOver120_CheckedChanged(sender As Object, e As EventArgs) Handles rdbOver120.CheckedChanged
        If rdbOver120.Checked = True Then
            txtAmount.Text = GlobalVariables.Over120Total
            txtAmount.Update()
        Else
            txtAmount.Text = "0.00"
            txtAmount.Update()
        End If
    End Sub

    Private Sub txtAccount_GotFocus(sender As Object, e As EventArgs) Handles txtAccount.GotFocus
        rdbCurrentOnly.Checked = False
        rdbCurrentOnly.Update()
    End Sub

    Private Sub rdbCurrent_CheckedChanged_1(sender As Object, e As EventArgs) Handles rdbCurrent.CheckedChanged
        If rdbCurrent.Checked = True Then
            txtAmount.Text = GlobalVariables.CurBillTotal
            txtAmount.Update()
        Else
            txtAmount.Text = "0.00"
            txtAmount.Update()
        End If
    End Sub

    Public Function decimalStringChecker(strValue As String) As String
        If String.IsNullOrEmpty(strValue) Then
            strValue = "0.00"
        End If
        Return strValue
    End Function
End Class

Public Class GlobalVariables
    Public Shared CurBillAmount As String = "0.00"
    Public Shared CurBillSurcharge As String = "0.00"
    Public Shared CurBillVat As String = "0.00"
    'Public Shared CurBillTotalVat As String = "0.00"
    Public Shared CurBillTotal As String = "0.00"

    Public Shared Over30Amount As String = "0.00"
    Public Shared Over30Surcharge As String = "0.00"
    Public Shared Over30Vat As String = "0.00"
    Public Shared Over30Total As String = "0.00"

    Public Shared Over60Amount As String = "0.00"
    Public Shared Over60Surcharge As String = "0.00"
    Public Shared Over60Vat As String = "0.00"
    Public Shared Over60Total As String = "0.00"

    Public Shared Over90Amount As String = "0.00"
    Public Shared Over90Surcharge As String = "0.00"
    Public Shared Over90Vat As String = "0.00"
    Public Shared Over90Total As String = "0.00"

    Public Shared Over120Amount As String = "0.00"
    Public Shared Over120Surcharge As String = "0.00"
    Public Shared Over120Vat As String = "0.00"
    Public Shared Over120Total As String = "0.00"

    Public Shared TotalArrears As String = "0.00"

    Public Shared CurBillDate As String = ""
    Public Shared Over30Date As String = ""
    Public Shared Over60Date As String = ""
    Public Shared Over90Date As String = ""
    Public Shared Over1200Date As String = ""

    Public Shared StatemenAccount = ""
End Class

Public Class WatermarkTextBox
    Inherits TextBox

    Private NotInheritable Class NativeMethods
        Private Sub New()
        End Sub

        Private Const ECM_FIRST As UInteger = &H1500
        Friend Const EM_SETCUEBANNER As UInteger = ECM_FIRST + 1

        <DllImport("user32.dll", EntryPoint:="SendMessageW")>
        Public Shared Function SendMessageW(hWnd As IntPtr, Msg As UInt32, wParam As IntPtr, <MarshalAs(UnmanagedType.LPWStr)> lParam As String) As IntPtr
        End Function
    End Class

    Private _watermark As String
    Public Property Watermark() As String
        Get
            Return _watermark
        End Get
        Set(value As String)
            _watermark = value
            UpdateWatermark()
        End Set
    End Property

    Private Sub UpdateWatermark()
        If IsHandleCreated AndAlso _watermark IsNot Nothing Then
            NativeMethods.SendMessageW(Handle, NativeMethods.EM_SETCUEBANNER, CType(1, IntPtr), _watermark)
        End If
    End Sub

    Protected Overrides Sub OnHandleCreated(e As EventArgs)
        MyBase.OnHandleCreated(e)
        UpdateWatermark()
    End Sub

End Class

Public Module TextBoxExtensions

    Private Const ECM_FIRST As UInteger = &H1500
    Private Const EM_SETCUEBANNER As UInteger = ECM_FIRST + 1

    <DllImport("user32.dll", EntryPoint:="SendMessageW")>
    Private Function SendMessageW(hWnd As IntPtr, Msg As UInt32, wParam As IntPtr, <MarshalAs(UnmanagedType.LPWStr)> lParam As String) As IntPtr
    End Function

    <Extension()>
    Public Sub SetWaterMark(tb As TextBox, watermark As String)
        SendMessageW(tb.Handle, EM_SETCUEBANNER, 0, watermark)
    End Sub

End Module



'''**************** PRINT *****************
'Dim selectedItem = cboPrinter.SelectedItem
'' Select the printer.
'PrintDocument1.PrinterSettings.PrinterName = selectedItem
'PrintHeader()
''ItemsToBePrinted()
''printFooter()

'Dim amount = Convert.ToDouble(resAmount)
'Dim doubleAmount = Math.Round(amount, 2)
'Dim finalAmount = doubleAmount.ToString("N", CultureInfo.InvariantCulture)

'Dim conFee = Convert.ToDouble(resService)
'Dim doubleConFee = Math.Round(conFee, 2)
'Dim finalFee = doubleConFee.ToString("N", CultureInfo.InvariantCulture)

'Dim surcharge = Convert.ToDouble(resSurcharge)
'Dim doubleSurcharge = Math.Round(surcharge, 2)
'Dim finalSurcharge = doubleSurcharge.ToString("N", CultureInfo.InvariantCulture)

'Dim total = Convert.ToDouble(resTotal)
'Dim doubleTotal = Math.Round(total, 2)
'Dim finalTotal = doubleTotal.ToString("N", CultureInfo.InvariantCulture)

'ItemsToBePrinted("Customer Account: ", resAccount)
'ItemsToBePrinted("Customer Name:    ", resName)
'ItemsToBePrinted("Customer Mobile:  ", resMobile)
'ItemsToBePrinted("Amount:           ", "Php" + finalAmount)
'ItemsToBePrinted("Surcharge:        ", "Php" + finalSurcharge)
'ItemsToBePrinted("Convenienve Fee:  ", "Php" + finalFee)
'ItemsToBePrinted("Billing Month:    ", resSOA)

'ItemsToBePrinted("", "") ' New line
''send Business Name
'Dim StringToPrint As String = "---------------------------------"
'Dim LineLen As Integer = StringToPrint.Length
'Dim spcLen1 As New String(" "c, Math.Round((52 - LineLen) / 2)) 'This line is used to center text in the middle of the receipt
'TextToPrint &= spcLen1 & StringToPrint & Environment.NewLine
'ItemsToBePrinted("", "") ' New line

'ItemsToBePrinted("Total:            ", "Php" + finalTotal)

'Dim printControl = New Printing.StandardPrintController
'PrintDocument1.PrintController = printControl
'Try
'PrintDocument1.Print()
'TextToPrint = ""
'Catch ex As Exception
'MsgBox(ex.Message)
'End Try